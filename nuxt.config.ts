import type { Config } from 'tailwindcss'
// import defaultTheme from 'tailwindcss/defaultTheme'`

// https://nuxt.com/docs/api/configuration/nuxt-config
export default <Partial<Config>>{
  runtimeConfig: {
    igToken: process.env.IG_TOKEN,
    public: {
      webTitle: 'Profile to landing web',
      igUsername: process.env.IG_USERNAME,
      igId: process.env.IG_ID,
    },
  },
  tailwindcss: {
    cssPath: '~/assets/css/main.css',
    configPath: 'tailwind.config',
  },
  modules: ['@nuxtjs/tailwindcss'],
  css: ['~/assets/css/main.css'],
}
