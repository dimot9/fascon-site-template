export default defineEventHandler( (event) => {
  const config = useRuntimeConfig();
  const url = `https://graph.facebook.com/v15.0/${config.public.igId}?fields=id,media_count,username,media,profile_picture_url,biography&access_token=${config.igToken}`;
  return $fetch(url, { method: "GET"});
});
