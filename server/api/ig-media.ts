export default defineEventHandler((event) => {
  const config = useRuntimeConfig();
  const query = getQuery(event)
  const url = `https://graph.facebook.com/v15.0/${query.mediaId}?fields=id,caption,media_type,media_url,permalink,children&access_token=${config.igToken}`;
  return $fetch(url, { method: "GET"});
});
