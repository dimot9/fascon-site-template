export const useIgApi = () => {
  const { igToken } = useRuntimeConfig()

  const getProfileInfo = async () => {
    // TODO: Types! Make type for a return object
    try {
      const { data, error } = await useFetch<any>(`/api/ig-profile`, {
        server: true,
      })

      return data
    } catch (e) {
      console.error('Failed to fetch IG profile.')
      return null
    }
  }

  const getMediasDetail = async (mediasIds: string[]) => {
    const medias = mediasIds.map(async (mediaId) => {
      const data = await $fetch(`/api/ig-media`, {
        query: {
          mediaId,
        },
      })
      return data
    })

    return Promise.all(medias)
  }

  return {
    getProfileInfo,
    getMediasDetail,
  }
}
